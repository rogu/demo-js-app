import { API_LOGIN_END_POINT, API_IS_LOGGED_IN_END_POINT, API_LOGOUT_END_POINT } from "../utils/constants";
import { LOG_IN_SUCCESS, LOG_OUT_SUCCESS } from "../utils/constants";
import Axios from "axios";

export class AuthModel {
    constructor() {
        this.isLoggedIn();
    }

    async logIn(formValue) {
        const { data } = await Axios.post(API_LOGIN_END_POINT, formValue);
        this.dispatch(data.ok);
    }

    async isLoggedIn() {
        const { data } = await Axios.get(API_IS_LOGGED_IN_END_POINT);
        this.dispatch(data.ok);
    }

    async logout() {
        await Axios.get(API_LOGOUT_END_POINT);
        this.dispatch(false);
    }

    dispatch(state) {
        state
            ? document.dispatchEvent(new CustomEvent(LOG_IN_SUCCESS))
            : document.dispatchEvent(new CustomEvent(LOG_OUT_SUCCESS))
    }
}
