import { AuthModel } from './auth-model';
import { AuthView } from './auth-view';
import { LOG_IN, LOG_OUT } from '../utils/constants';

export class AuthController {
    constructor() {
        this.model = new AuthModel();
        this.view = new AuthView(document.querySelector('.auth-container'));

        this.view.container.addEventListener(LOG_IN, (evt) => this.model.logIn(evt.detail));
        this.view.container.addEventListener(LOG_OUT, () => this.model.logout());
    }
}
