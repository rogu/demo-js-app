import { LOG_IN, LOG_IN_SUCCESS, LOG_OUT, LOG_OUT_SUCCESS } from "../utils/constants";

export class AuthView {
    constructor(container) {
        this.container = container;
        this.render();
        this.form = this.container.querySelector('form');
        this.btnLogIn = this.form.querySelector('button');
        this.btnLogout = this.container.querySelector('.btn-logout');

        this.form.addEventListener('submit', (evt) => this.logIn(evt));
        this.btnLogout.addEventListener('click', (evt) => this.logOut());
        document.addEventListener(LOG_IN_SUCCESS, () => this.toggleState(true));
        document.addEventListener(LOG_OUT_SUCCESS, () => this.toggleState(false));
    }

    toggleState(state) {
        this.form.classList[state ? 'add' : 'remove']('d-none');
        this.btnLogout.classList[state ? 'remove' : 'add']('d-none');
    }

    logIn(evt) {
        evt.preventDefault();
        if (evt.target.checkValidity()) {
            const detail = {
                username: evt.target.username.value,
                password: evt.target.password.value
            };
            this.container.dispatchEvent(new CustomEvent(LOG_IN, { detail }));
        }
    }

    logOut() {
        this.container.dispatchEvent(new CustomEvent(LOG_OUT));
    }

    render() {
        this.container.innerHTML = `
          <form class="form-inline d-none">
                <input class="form-control form-control-sm"
                       type="email"
                       name="username"
                       required
                       value="admin@localhost"
                       placeholder="username">
                <input class="form-control form-control-sm"
                        required
                       type="password"
                       name="password"
                       value="admin"
                       placeholder="password">
                <button class="btn btn-sm btn-primary">
                    log in
                </button>
            </form>
            <button class="btn-logout btn-sm btn btn-danger d-none">log out</button>
        `;
    }
}
