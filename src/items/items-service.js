import { API_ITEMS_END_POINT } from '../utils/constants';
import Axios from 'axios';

export class ItemsService {

    makeParams(params) {
        if (!params) return;
        return '?' + Object.keys(params)
            .map((key) => {
                return key + "=" + params[key]
            }).join("&");
    }

    fetch(params) {
        return Axios.get(`${API_ITEMS_END_POINT}${this.makeParams(params)}`);
    }

    remove(id) {
        return Axios.delete(`${API_ITEMS_END_POINT}/${id}`);
    }

    add(item) {
        return Axios.post(API_ITEMS_END_POINT, item);
    }
}
