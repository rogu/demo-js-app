import { ItemsService } from './items-service';
import { SEARCH_ITEM, REMOVE_ITEM, ADD_ITEM } from '../utils/constants';
import { ListView } from './views/list-view';
import { SearchView } from './views/search-view';
import { AddItemView } from './views/add-item-view';

export class ItemsController {
    constructor() {
        this.listView = new ListView(
            document.querySelector('.list-container'),
            document.querySelector('.pagination-container'),
            [['title', 'text'], ['price', 'input'], ['imgSrc', 'image']]
        );
        this.model = new ItemsService();
        this.searchView = new SearchView(document.querySelector('.search-container'));
        this.addItemView = new AddItemView(document.querySelector('.add-item-container'));

        this.filters = {
            itemsPerPage: 5,
            currentPage: 1
        };

        this.fetchItems();

        this.listView.itemsContainer.addEventListener(REMOVE_ITEM, (evt) => this.removeItem(evt));
        this.listView.paginationContainer.addEventListener('change', evt => this.changePage(evt));
        this.addItemView.container.addEventListener(ADD_ITEM, evt => this.addItem(evt));
        this.searchView.container.addEventListener(SEARCH_ITEM, evt => this.searchItems(evt))
    }

    changePage({ target: { value: detail } }) {
        this.filters.currentPage = +detail;
        this.fetchItems();
    }

    searchItems({ detail }) {
        this.filters = { ...this.filters, ...detail, currentPage: 1 }
        this.fetchItems();
    }

    addItem({ detail }) {
        this.model
            .add(detail)
            .then(() => this.fetchItems())
    }

    removeItem({ detail }) {
        if (confirm('are you sure?'))
            this.model
                .remove(detail)
                .then(() => this.fetchItems())
    }

    fetchItems() {
        this.model
            .fetch(this.filters)
            .then(({ data }) => this.listView.render(data, this.filters));
    }
}
