import { SEARCH_ITEM } from "../../utils/constants";

export class SearchView {
    constructor(container) {
        this.container = container;
        this.render();
        this.container.querySelector('input[name=title]')
            .addEventListener('input', ({ target }) => this.dispatch(target));
        this.container.querySelector('[name=itemsPerPage]')
            .addEventListener('change', ({ target }) => this.dispatch(target));
    }

    dispatch({ name, value }) {
        const detail = { [name]: value };
        this.container.dispatchEvent(new CustomEvent(SEARCH_ITEM, { detail }));
    }

    render() {
        this.container.innerHTML = `
            <h3>search</h3>
            <form id="search-form" class="card card-body">
                <div class="form-group">
                    <input type="text"
                    class="form-control"
                    name="title"
                    placeholder="search by title">
                </div>

                <div class="form-group">
                <h6>items per page</h6>
                    <select name="itemsPerPage" class="form-control">
                        <option selected disabled>items per page</option>
                        <option value="2">2</option>
                        <option value="5">5</option>
                        <option value="10">10</option>
                    </select>
                </div>
            </form>
        `
    }
}
