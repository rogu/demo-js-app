import { Validate } from '../../utils/validate'
import { ADD_ITEM } from '../../utils/constants';

export class AddItemView {

    constructor(container) {
        this.container = container;
        this.render();

        const form = this.container.querySelector('form[name=addItemForm]'),
            modal = this.container.querySelector('.add-item-modal'),
            btnOpen = this.container.querySelector(".open-modal-btn"),
            btnClose = this.container.querySelector('.close-modal-btn');

        /**
         * modal events
         */
        btnOpen.addEventListener('click', () => this.setVisible(modal, "block"));
        btnClose.addEventListener('click', () => this.setVisible(modal, "none"));
        window.addEventListener('click', ({ target }) => target === modal && this.setVisible(modal, "none"));

        /**
         * form events
         */
        form.querySelector('button').addEventListener('click', () => this.checkAndSend(form, modal));
    }

    checkAndSend(form, modal) {
        const validate = new Validate(form);
        validate.checkForm((data) => {
            this.container.dispatchEvent(new CustomEvent(ADD_ITEM, { detail: data }));
            modal.style.display = "none";
        });
    }

    setVisible(modal, value) {
        modal.style.display = value;
    }

    render() {
        this.container.innerHTML = `
            <button class="btn btn-primary open-modal-btn">add item</button>
            <div class="modal add-item-modal">
                <div class="modal-content">
                    <div>
                        ADD ITEM
                        <span class="close-modal-btn">&times;</span>
                    </div>
                    <form name="addItemForm" class="">
                        <div class="form-group">
                            <select name="category"
                                    required
                                    class="form-control form-control-sm">
                                <option value="" selected disabled>category</option>
                                <option value="food">food</option>
                                <option value="clothes">clothes</option>
                            </select>
                        </div>
                        ${[{ name: 'title', type: 'text' }, { name: 'price', type: 'number' }].map((item) => {
                return `<div class="form-group">
                                        <input required
                                               class="form-control form-control-sm"
                                               placeholder="${item.name}"
                                               type="${item.type}"
                                               name="${item.name}">
                                    </div>`
            }).join('')}
                        <button type="button" class="btn btn-primary" id="addItem">send</button>
                    </form>
                </div>
            </div>
                `;
    }
}
