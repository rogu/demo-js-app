import { REMOVE_ITEM } from "../../utils/constants";

export class ListView {

    constructor(itemsContainer, paginationContainer, config) {
        this.itemsContainer = itemsContainer;
        this.paginationContainer = paginationContainer;
        this.itemsConfig = config;
        this.itemsContainer.addEventListener('click', this.removeItem.bind(this));
    }

    render({ data, total }, filters) {
        this.createPagination(total, filters);

        this.itemsContainer.innerHTML = `
            <h3>Items list</h3>
            ${this.createTable(data)}
        `;
    }

    createTag(item, setting) {
        switch (setting[1]) {
            case 'input':
                return `<input value="${item[setting[0]]}" class="form-control">`
            case 'image':
                return `<img src="${item[setting[0]]}" width="50">`
            default:
                return item[setting[0]];
        }
    }

    createTable(data) {
        return `
            <table class="list-container table table-bordered table-striped">
                <thead>
                    <tr>
                        ${this.itemsConfig.map((head) => {
                return `<th>${head[0].toUpperCase()}</th>`
            }).join('')}
                        <th>ACTION</th>
                    </tr>
                </thead>
                <tbody>
                    ${data.map((item) => {
                return `<tr>
                            ${this.itemsConfig.map((conf) => {
                        return `<td>${this.createTag(item, conf)}</td>`
                    }).join('')}
                            <td><button data-id="${item.id}" class="btn btn-danger btn-sm">remove</button></td>
                        </tr>`
            }).join('')}
                </tbody>
            </table>
        `;
    }

    removeItem({ target: { tagName, dataset } }) {
        if (tagName === 'BUTTON')
            this.itemsContainer.dispatchEvent(new CustomEvent(REMOVE_ITEM, { detail: dataset.id }));
    }

    createPagination(total, { itemsPerPage, currentPage }) {
        const pages = Math.ceil(total / itemsPerPage);
        let bricks = new Array(pages).join().split(',');
        this.paginationContainer.innerHTML = `
            ${bricks.map((item, idx) => {
                return `
                    <label class="btn btn-primary btn-sm ${(currentPage - 1) === idx ? 'active' : ''}">
                                ${idx + 1}
                                <input type="radio"
                                    name="pagination"
                                    value="${idx + 1}">
                    </label>
                `
            }).join('')}
            <div class="badge badge-info">total: ${total}</div>
        `;
    }
}
