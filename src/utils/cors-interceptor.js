import Axios from 'axios';

Axios.interceptors.request.use(
    (req) => ({ ...req, withCredentials: true }),
    (error) => Promise.reject(error)
);

Axios.interceptors.response.use(
    (resp) => resp,
    (error) => {
        alert(`SERVER ERROR\n${JSON.stringify(error.response, null, 4)}`);
        return Promise.reject(error);
    });
