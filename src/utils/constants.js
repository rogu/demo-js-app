/**
 * custom events
 */
export const REMOVE_ITEM = 'REMOVE_ITEM';
export const ADD_ITEM = 'ADD_ITEM';
export const SEARCH_ITEM = 'SEARCH_ITEM';
export const CHANGE_PAGE = 'CHANGE_PAGE';
export const LOG_IN = 'LOG_IN';
export const LOG_IN_SUCCESS = 'LOG_IN_SUCCESS';
export const LOG_OUT = 'LOG_OUT';
export const LOG_OUT_SUCCESS = 'LOG_OUT_SUCCESS';

/**
 * api end points
 */
export const API_BASE = 'https://api.debugger.pl/';
export const API_ITEMS_END_POINT = `${API_BASE}items`;

export const API_AUTH_BASE = 'https://auth.debugger.pl/';
export const API_LOGIN_END_POINT = `${API_AUTH_BASE}login`;
export const API_LOGOUT_END_POINT = `${API_AUTH_BASE}logout`;
export const API_IS_LOGGED_IN_END_POINT = `${API_AUTH_BASE}logged`;
