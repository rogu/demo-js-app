import {ItemsController} from './items/items-controller';
import {AuthController} from './auth/auth-controller';
import './utils/cors-interceptor';

new ItemsController();
new AuthController();
